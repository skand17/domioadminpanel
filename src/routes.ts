import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard'));
const Users = React.lazy(() => import('./views/Users/user'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config

// Admin panel routes
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/users', name: 'Dashboard', component: Users },
];

export default routes;
