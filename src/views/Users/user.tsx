import React, { Component } from 'react';

interface State  {
    count : Number
}

export class Users extends Component<State> {

    state: State = {
        count: 290
    };

    render() {

        console.log(this.state.count)

        return (
            <div>
                Users
            </div>
        );
    }
}

export default Users;
