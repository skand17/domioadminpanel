import React, { Component } from 'react';

interface State  {
    count : Number
}

export class Dashboard extends Component<State> {

    state: State = {
        count: 290
    };

    render() {

        console.log(this.state.count)

        return (
            <div>
                Hello
            </div>
        );
    }
}

export default Dashboard;
